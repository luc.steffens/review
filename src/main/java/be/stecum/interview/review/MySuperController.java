package be.stecum.interview.review;

import java.util.Enumeration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.servlet.http.HttpServletRequest;

public class MySuperController {

    /**
     * Fetch a map with CustomHeaders
     * @param request
     * @return map with headers and value
     */
    Map<String, String> fetchHeaderMap(HttpServletRequest request) {
        Map<String, String> headerMapper = new ConcurrentHashMap<String, String>();
        Enumeration<String> names = request.getHeaderNames();

        while (names.hasMoreElements()) {
            String name = (String)names.nextElement();
            Enumeration<String> values = request.getHeaders(name);
            if (values != null) {
                while (values.hasMoreElements()) {
                    String value = (String)values.nextElement();
                    if (CustomHeader.MY_CUSTUM_ID.httpHeaderValue.equalsIgnoreCase(name)) {
                        headerMapper.put(CustomHeader.MY_CUSTUM_ID.httpHeaderValue(), value);
                    }
                    if (CustomHeader.SOME_REFERENCE.httpHeaderValue.equalsIgnoreCase(name)) {
                        headerMapper.put(CustomHeader.SOME_REFERENCE.httpHeaderValue(), value);
                    }
                    if (CustomHeader.USER_REF.httpHeaderValue.equalsIgnoreCase(name)) {
                        headerMapper.put(CustomHeader.USER_REF.httpHeaderValue(), value);
                    }
                    if (CustomHeader.EMPLOYEE.httpHeaderValue.equalsIgnoreCase(name)) {
                        headerMapper.put(CustomHeader.EMPLOYEE.httpHeaderValue(), value);
                    }
                }
            }
        }

        return headerMapper;
    }

    Map<String, String> fetchHeaderMapBetterImplementation(HttpServletRequest request) {
        throw new UnsupportedOperationException("Starting right on!!"); 
    }

    enum CustomHeader {
        MY_CUSTUM_ID("X-Cust-Id"),
        SOME_REFERENCE("X-Some-Ref"),
        USER_REF("Y-UserRef"),
        EMPLOYEE("X-EmplId");

        private final String httpHeaderValue;

        private CustomHeader(String httpHeaderValue) {
            this.httpHeaderValue = httpHeaderValue;
        }

        public String httpHeaderValue() {
            return httpHeaderValue;
        }

    }
}
